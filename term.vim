func! Term()
    if get(g:, 'term') && bufnr() != g:term
        split
        exe "buffer". g:term
        return 0
    endif
    if get(g:, 'term') && bufnr() == g:term
        close
        return 0
    endif
    term
endfunc
augroup term
    au!
    au TerminalOpen,TerminalWinOpen * let g:term = bufnr()
augroup END
