runtime! ./plugconf/*.vim
let g:ale_disable_lsp = 1
call plug#begin()
    Plug 'itchyny/lightline.vim'
    Plug 'ghifarit53/tokyonight-vim'
    Plug 'neovimhaskell/haskell-vim'
    Plug 'scrooloose/nerdtree'
    Plug 'ryanoasis/vim-devicons'
    Plug 'zah/nim.vim'
    Plug 'mengelbrecht/lightline-bufferline'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'dense-analysis/ale'
    Plug 'maximbaz/lightline-ale'
    Plug 'tpope/vim-fugitive'
call plug#end()
