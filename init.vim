syntax on
filetype plugin indent on
set hid
set enc=utf-8
set tgc
set nu rnu
set sb
set et
set sw=4 
set si
set sta
set ls=2
set stal=2
set scl=yes
set to
set tm=100
set bg=dark
set t_Cs="\e[4:3m"
set t_Ce="\e[4:0m"
set t_Us="\e[4:2m"
set t_ds="\e[4:4m"
set t_Ds="\e[4:5m"
so $HOME/.vim/plugins.vim
so $HOME/.vim/map.vim
so $HOME/.vim/term.vim
colorscheme tokyonight
