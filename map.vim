let mapleader = ' '
nnoremap <leader>w :wa<CR>
nnoremap <leader>q :confirm qa<CR>
nnoremap <C-t> :call Term()<CR>
tnoremap <Esc> <C-\><C-n>
tnoremap <C-t> <C-\><C-n> :call Term()<CR>
